package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Dalija on 20.5.2018..
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MojResultReceiver.Receiver {
    Spinner kategorije;
    EditText upit;
    Spinner rezultat;
    Button pokreni;
    Button dodaj;
    Button povratak;
    private OnBackPress1 pomocna;
    ArrayList<Knjiga> naziviKnjiga = new ArrayList<>();
    Custom aa2;
    ArrayAdapter aa;
    ArrayList<String> listica = new ArrayList<String>();
    int poz = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online, containet, false);
        kategorije = (Spinner) view.findViewById(R.id.sKategorije);
        upit = (EditText) view.findViewById(R.id.tekstUpit);
        rezultat = (Spinner) view.findViewById(R.id.sRezultat);
        pokreni = (Button) view.findViewById(R.id.dRun);
        dodaj = (Button) view.findViewById(R.id.dAdd);
        povratak = (Button) view.findViewById(R.id.dPovratak);
        pomocna = (OnBackPress1) getActivity();

        Bundle bundle = getArguments();

        listica = bundle.getStringArrayList("kategorije");
        aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listica);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorije.setAdapter(aa);

        aa2 = new Custom(getActivity(), naziviKnjiga);
        rezultat.setAdapter(aa2);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pomocna.onBackPressed1();
            }
        });


        pokreni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = upit.getText().toString();
                if(s.contains("autor:")){
                    s = s.substring(6, s.length());
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(s);
                }
                if(s.contains(";")){
                    ArrayList<String> temp = new ArrayList<>();
                    String[] arr = s.split(";");
                    for(int i=0; i<arr.length; i++){
                        String pomocni = arr[i].toString();
                        temp.add(pomocni);
                    }

                    for(int i=0; i<temp.size(); i++){
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(temp.get(i).toString());
                    }
                }
                if(s.contains("korisnik:")){
                    s = s.substring(9, s.length());
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    MojResultReceiver mReceiver = new MojResultReceiver(new Handler());
                    mReceiver.setmReceiver(FragmentOnline.this);

                    intent.putExtra("idKorisnika", s);
                    intent.putExtra("receiver", mReceiver);

                    getActivity().startService(intent);
                }
                else{
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(s);
                }
            }
        });

        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poz = rezultat.getSelectedItemPosition();

                Knjiga pom = naziviKnjiga.get(poz);
                pom.zanr = kategorije.getSelectedItem().toString();
                PomocnaB.listaKnjiga.add(pom);
                upit.setText("");
            }
        });

        return view;
    }

    public void onDohvatiDone(ArrayList<Knjiga> k){
        for(int i=0; i<k.size(); i++){
            naziviKnjiga.add(k.get(i));
        }
        aa2.notifyDataSetChanged();
    }

    public void onNajnovijeDone(ArrayList<Knjiga> k){
        for(int i=0; i<k.size(); i++){
            naziviKnjiga.add(k.get(i));
        }
        aa2.notifyDataSetChanged();
    }

    public interface OnBackPress1{
        public void onBackPressed1();
    }

    public void onReceiveResult(int resultCode, Bundle resultData){
        if(resultCode == 0){
            Toast.makeText(getActivity(), "Servis je poceo s radom", Toast.LENGTH_LONG).show();
        }
        if(resultCode == 1){
            naziviKnjiga.add((Knjiga) resultData.get("result"));
            aa2.notifyDataSetChanged();
        }
        else{
            Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
        }
    }
}
