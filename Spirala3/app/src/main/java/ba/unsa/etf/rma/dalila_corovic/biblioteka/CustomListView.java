package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomListView extends BaseAdapter{
    private Context kontekst;
    ArrayList<Knjiga> pomocna;

    public CustomListView(Context context, ArrayList<Knjiga> data) {
        this.kontekst = context;
        this.pomocna=data;
    }

    @Override
    public int getCount() {
        return pomocna.size();
    }

    @Override
    public Object getItem(int i) {
        return pomocna.get(i).getAutor();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.element_liste_knjiga, null);
            }

            TextView tt1 = (TextView) convertView.findViewById(R.id.eNaziv);
            TextView tt2 = (TextView) convertView.findViewById(R.id.eAutor);
            ImageView im = (ImageView) convertView.findViewById(R.id.eNaslovna);

            tt1.setText(pomocna.get(position).getNazivknjige());
            tt2.setText(pomocna.get(position).getAutor());
            im.setImageBitmap(pomocna.get(position).getSlikanaslovna());


            if (pomocna.get(position).background==1) convertView.setBackgroundColor(0xffaabbed);

            return convertView;
        }
}




