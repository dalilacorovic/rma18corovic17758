package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class KategorijeAkt extends AppCompatActivity implements ListeFragment.OnButtonClick2, FragmentOnline.OnBackPress1, ListeFragment.OnButtonClick, DodavanjeKnjigeFragment.OnBackPress, KnjigeFragment.OnBackPress, ListeFragment.OnListClick{
    Button dugme1;
    Button dugme2;
    Button dugme3;
    Button dugmeonline;
    EditText tekst;
    ListView lista;
    ArrayAdapter<String> adapter;
    ArrayList<String> unosi;
    FragmentManager fm = getFragmentManager();
    Boolean siril=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);

        dugme1 = (Button) findViewById(R.id.dDodajKategoriju);
        dugme2 = (Button) findViewById(R.id.dPretraga);
        dugme3 = (Button) findViewById(R.id.dDodajKnjigu);
        tekst = (EditText) findViewById(R.id.tekstPretraga);
        lista = (ListView) findViewById(R.id.listaKategorija);
        dugmeonline = (Button) findViewById(R.id.dDodajOnline);

//        dugme1.setEnabled(false);

        unosi = new ArrayList<String>();
 /*     adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, unosi);
        lista.setAdapter(adapter);

        //PRETRAGA - filtriranje
        dugme2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String string = tekst.getText().toString();
                (KategorijeAkt.this).adapter.getFilter().filter(string);
                if(adapter.getCount()==0) {
                    dugme1.setEnabled(true);
                }
            }
        });

        //DODAJ KATEGORIJU
        dugme1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                unosi.add(0,tekst.getText().toString());
                adapter.add(tekst.getText().toString());
                adapter.notifyDataSetChanged();
                tekst.setText("");
            }
        });

        //DODAJ KNJIGU AKTIVNOST
        dugme3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KategorijeAkt.this, DodavanjeKnjigeAkt.class);
                intent.putStringArrayListExtra("kategorije", unosi);
                KategorijeAkt.this.startActivity(intent);
            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                myIntent.putExtra("kategorija", unosi.get(i));
                KategorijeAkt.this.startActivity(myIntent);

            }
        });

        */
/*

        FrameLayout pocetni = (FrameLayout) findViewById(R.id.prikaziFragment);

        if (pocetni != null) {
            ListeFragment lf = (ListeFragment) fm.findFragmentById(R.id.prikaziFragment);
            if(lf==null){
                lf = new ListeFragment();
                fm.beginTransaction().replace(R.id.prikaziFragment, lf).commit();
            }
        }
*/
        FragmentManager fm = getFragmentManager();
        FrameLayout ldeatelji = (FrameLayout)findViewById(R.id.mjestoF2);
        if(ldeatelji!=null){
            siril = true;
            KnjigeFragment lf = (KnjigeFragment) fm.findFragmentById(R.id.mjestoF2);
            if(lf==null){
                lf = new KnjigeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("kategorija", "kat");
                lf.setArguments(bundle);

                fm.beginTransaction().replace(R.id.mjestoF2, lf).commit();
            }
            else {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

        ListeFragment kf = (ListeFragment)fm.findFragmentByTag("Lista");
        if(kf==null){
            kf = new ListeFragment();
            fm.beginTransaction().replace(R.id.prikaziFragment, kf, "Lista").commit();
        }

    }

    @Override
    public void OnButtonClicked(ArrayList<String> unos){
        DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("kategorije", unos);
        dkf.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, dkf).addToBackStack("prva").commit();

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void OnListClicked(String s){
        KnjigeFragment kf = new KnjigeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("kategorija", s);
        kf.setArguments(bundle);

        if(siril){
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, kf).commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, kf).addToBackStack("druga").commit();
        }

    }

    @Override
    public void OnButtonClicked2(ArrayList<String> unos){
        FragmentOnline fo = new FragmentOnline();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("kategorije", unos);
        fo.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, fo).addToBackStack("treci").commit();
    }

    @Override
    public void onBackPressed1(){
        getFragmentManager().popBackStack();
    }

}
