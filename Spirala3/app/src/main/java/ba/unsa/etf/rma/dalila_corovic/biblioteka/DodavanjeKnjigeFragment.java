package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.icu.util.BuddhistCalendar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;


public class DodavanjeKnjigeFragment extends Fragment {
    static final int PICK_IMAGE_REQUEST = 1;
    ImageView slika;
    EditText autor;
    EditText knjiga;
    Button nadjisliku;
    Button upisiknjigu;
    Spinner spin;
    Button ponisti;
    ArrayAdapter aa;
    private OnBackPress pom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.activity_dodavanje_knjige_akt, containet, false);

        slika = (ImageView)view.findViewById(R.id.naslovnaStr);
        autor = (EditText)view.findViewById(R.id.imeAutora);
        knjiga = (EditText)view.findViewById(R.id.nazivKnjige);
        nadjisliku = (Button)view.findViewById(R.id.dNadjiSliku);
        upisiknjigu = (Button)view.findViewById(R.id.dUpisiKnjigu);
        spin = (Spinner)view.findViewById(R.id.sKategorijaKnjige);
        ponisti = (Button)view.findViewById(R.id.dPonisti);
        pom = (OnBackPress)getActivity();


        Bundle bundle = getArguments();
        ArrayList<String> listica = new ArrayList<String>();
        listica = bundle.getStringArrayList("kategorije");
        aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listica);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        upisiknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String i; //ime autora
                    String k; //knjiga
                    String z; //zanr-kategorija
                    Bitmap b;
                    i = autor.getText().toString();
                    k = knjiga.getText().toString();
                    z = spin.getSelectedItem().toString();
                    slika.setDrawingCacheEnabled(true);
                    slika.buildDrawingCache();
                    b = Bitmap.createBitmap(slika.getDrawingCache());

                    Knjiga pom = new Knjiga(i, k, z, b);
                    PomocnaB.listaKnjiga.add(pom);

                    autor.setText("");
                    knjiga.setText("");
                }
                catch (Exception e){
                    Toast.makeText(getActivity().getApplicationContext(), "Ne mozete dodati knjigu bez kategorije!", Toast.LENGTH_LONG).show();
                }
            }
        });

        nadjisliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivity(intent);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom.onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        DodavanjeKnjigeFragment.super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                slika.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnBackPress{
        public void onBackPressed();
    }
}
