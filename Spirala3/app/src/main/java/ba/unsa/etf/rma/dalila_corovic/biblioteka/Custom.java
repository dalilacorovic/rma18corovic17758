package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Dalija on 21.5.2018..
 */

public class Custom extends BaseAdapter {
    private Context kontekst;
    ArrayList<Knjiga> pomocna;

    public Custom(Context context, ArrayList<Knjiga> data) {
        this.kontekst = context;
        this.pomocna=data;
    }

    @Override
    public int getCount() {
        return pomocna.size();
    }

    @Override
    public Object getItem(int i) {
        return pomocna.get(i).getAutor();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.element_liste_knjiga, null);
        }

        TextView tt1 = (TextView) convertView.findViewById(R.id.eNaziv);
        tt1.setText(pomocna.get(position).getNaziv());

        return convertView;
    }
}
