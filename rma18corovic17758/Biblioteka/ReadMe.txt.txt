+++++ Dalila �orovi�
+++++ 17758
+++++ 2017/18
+++++ RMA
+++++
+++++ Spirala 1
+++++ Ura�eno: 
+++++ lista ura�enih zadataka i dijelova zadataka:

Projektom koji �aljem u prilog, obuhvatila sam sve zadatke navedene u postavci spirale.
Pod tim podrazumijevam kreiranje polazne aktivnosti sa unaprijed zadatim layoutom (polje pretrage, 
dugme za pretragu, dodavanje kategorije i knjige, te spisak kategorija). U okviru kreirane aktivnosti,
uklju�eno je filtriranje liste klikom na dugme dPretraga. Dodavanje kategorije u listu klikom na dugme dDodajKategoriju,
odnosno pokretanje nove aktivnosti za unos knjiga, klikom na dugme dDodajKnjigu. 
Kada je rije� o toj aktivnosti, njen layout je pode�en na osnovu vlastite konfiguracije kao �to i tekst zadatka nala�e.
Shodno tome, uklju�uje naziv autora knjige, naziv knjige, slika naslovne strane, dugme za nala�enje slike naslovne strane knjige,
dugme za upis knjige, povratak na po�etnu aktivnost, odnosno spinner za odabir kategorije u kojoj knjigu �elimo svrstati.
Radi pam�enja unesene knjige, kreirala sam pomo�nu klasu PomocnaB (/*pomo�na biblioteka*/) sa�injenu od stati�ne ArrayList tipa Knjiga.
Na taj na�in posti�emo pristup iz svake aktivnosti ovoj listi koja tokom rada aplikacije ostaje sa�injena od knjiga koje ubacimo u nju 
bez bojazni da se na�im prelaskom iz aktivnosti u neku drugu aktivnost (ili obratno) izgube podaci knjiga koje smo unijeli.
Klasa Knjiga sa�injena je od atributa: String autor, String nazivknjige, String zanr, Bitmap slikanaslovna i int background (koja �e nam
kasnije poslu�iti za pam�enje boje pozadine elementa liste)
Na po�etnoj aktivnosti ukoliko pritisnemo kategoriju otvaramo novu aktivnost koja �e prikazati spisak knjiga koje pripadaju toj kategoriji.
Intentom koji spaja ove dvije aktivnosti, proslje�ujem String kategorije. U aktivnosti ListaKnjigaAkt sa�uvamo u String varijablu kategoriju koja
nam je proslije�ena intentom te napraviti pomo�nu listu Knjiga koja �e biti sadr�ana samo od onih knjiga koje pripadaju toj kategoriji. 
Neophodan je i Custem list view koji �e konfigurisati prikaz tih elemenata liste i uskladiti ih prema tra�enom obliku. U adapter liste proslje�uje se app context i 
pomo�na filtrirana lista elemenata koji su zadovoljili uslov da se nalaze u konfigurisanoj kategoriji.
Za dugme dNadjiSliku dodan je implicitni intent i ACTION_GET_CONTENT za nala�enje slike za naslovnu stranu knjige, prilikom dodavanje knjige.
Za kraj, klikom na listu knjiga pozadina item/knjige poprima svijetlo plavu boju, te takvo stanje ostaje upam�eno postavljanjem atributa (background) te knjige
na vrijednost 1 (obojeno) �to se dojavljuje adapteru liste. 



