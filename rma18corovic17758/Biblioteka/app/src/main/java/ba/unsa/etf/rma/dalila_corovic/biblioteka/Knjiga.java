package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by Dalija on 27.3.2018..
 */

public class Knjiga {
    public String autor;
    public String nazivknjige;
    public String zanr;
    public Bitmap slikanaslovna;
    public int background;

    public Knjiga(String autor, String nazivknjige, String zanr, Bitmap slikanaslovna) {
        this.autor = autor;
        this.nazivknjige = nazivknjige;
        this.zanr = zanr;
        this.slikanaslovna = slikanaslovna;
        this.background=0;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getNazivknjige() {
        return nazivknjige;
    }

    public void setNazivknjige(String nazivknjige) {
        this.nazivknjige = nazivknjige;
    }

    public String getZanr() {
        return zanr;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }

    public Bitmap getSlikanaslovna() {
        return slikanaslovna;
    }

    public void setSlikanaslovna(Bitmap slikanaslovna) {
        this.slikanaslovna = slikanaslovna;
    }

}
