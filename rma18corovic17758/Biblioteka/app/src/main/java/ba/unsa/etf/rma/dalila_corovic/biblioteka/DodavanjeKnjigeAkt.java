package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.util.ArrayList;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
    ImageView slika;
    EditText autor;
    EditText knjiga;
    Button nadjisliku;
    Button upisiknjigu;
    Spinner spin;
    Button ponisti;
    ArrayList<String> lista;
    ArrayList<Knjiga> knjige;
    static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);

        slika = (ImageView)findViewById(R.id.naslovnaStr);
        autor = (EditText)findViewById(R.id.imeAutora);
        knjiga = (EditText)findViewById(R.id.nazivKnjige);
        nadjisliku = (Button)findViewById(R.id.dNadjiSliku);
        upisiknjigu = (Button)findViewById(R.id.dUpisiKnjigu);
        spin = (Spinner)findViewById(R.id.sKategorijaKnjige);
        ponisti = (Button)findViewById(R.id.dPonisti);

        lista = new ArrayList<String>();
        knjige = new ArrayList<Knjiga>();
        lista = getIntent().getStringArrayListExtra("kategorije");

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item, lista);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        upisiknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String i; //ime autora
                String k; //knjiga
                String z; //zanr-kategorija
                Bitmap b;
                i = autor.getText().toString();
                k = knjiga.getText().toString();
                z = spin.getSelectedItem().toString();
                slika.setDrawingCacheEnabled(true);
                slika.buildDrawingCache();
                b = Bitmap.createBitmap(slika.getDrawingCache());

                Knjiga pom = new Knjiga(i, k, z, b);
                PomocnaB.listaKnjiga.add(pom);

                autor.setText("");
                knjiga.setText("");
            }
        });


        nadjisliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivity(intent);
                ///
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        DodavanjeKnjigeAkt.super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                slika.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
