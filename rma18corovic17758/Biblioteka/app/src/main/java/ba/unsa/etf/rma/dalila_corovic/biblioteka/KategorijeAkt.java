package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class KategorijeAkt extends AppCompatActivity {
    Button dugme1;
    Button dugme2;
    Button dugme3;
    EditText tekst;
    ListView lista;
    ArrayAdapter<String> adapter;
    ArrayList<String> unosi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);

        dugme1 = (Button)findViewById(R.id.dDodajKategoriju);
        dugme2 = (Button)findViewById(R.id.dPretraga);
        dugme3 = (Button)findViewById(R.id.dDodajKnjigu);
        tekst = (EditText) findViewById(R.id.tekstPretraga);
        lista = (ListView)findViewById(R.id.listaKategorija);

        dugme1.setEnabled(false);

        unosi = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, unosi);
        lista.setAdapter(adapter);

        //PRETRAGA - filtriranje
        dugme2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String string = tekst.getText().toString();
                (KategorijeAkt.this).adapter.getFilter().filter(string);
                if(adapter.getCount()==0) {
                    dugme1.setEnabled(true);
                }
            }
        });

        //DODAJ KATEGORIJU
        dugme1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                unosi.add(0,tekst.getText().toString());
                adapter.add(tekst.getText().toString());
                adapter.notifyDataSetChanged();
                tekst.setText("");
            }
        });

        //DODAJ KNJIGU AKTIVNOST
        dugme3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KategorijeAkt.this, DodavanjeKnjigeAkt.class);
                intent.putStringArrayListExtra("kategorije", unosi);
                KategorijeAkt.this.startActivity(intent);
            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent myIntent = new Intent(KategorijeAkt.this, ListaKnjigaAkt.class);
                myIntent.putExtra("kategorija", unosi.get(i));
                KategorijeAkt.this.startActivity(myIntent);

            }
        });
    }
}
