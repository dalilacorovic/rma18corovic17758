package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomSpinner extends BaseAdapter{
    private Context kontekst;
    ArrayList<Kontakt> pomocna;

    public CustomSpinner(Context context, ArrayList<Kontakt> data) {
        this.kontekst = context;
        this.pomocna = data;
    }


    @Override
    public int getCount() {
        return pomocna.size();
    }

    @Override
    public Object getItem(int i) {
        return pomocna.get(i).getIme();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.spinner_kon, null);
        }

        TextView tt1 = (TextView) convertView.findViewById(R.id.ime);
        TextView tt2 = (TextView) convertView.findViewById(R.id.email);

        tt1.setText(pomocna.get(position).getIme());
        tt2.setText(pomocna.get(position).getEmail());

        return convertView;
    }
}
