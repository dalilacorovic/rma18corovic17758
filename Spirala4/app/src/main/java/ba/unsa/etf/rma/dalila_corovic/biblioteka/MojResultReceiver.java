package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by Dalija on 21.5.2018..
 */

public class MojResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public MojResultReceiver(Handler handler){
        super(handler);
    }

    public void setmReceiver(Receiver receiver){
        mReceiver = receiver;
    }

    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    protected void onReceiveResult(int resultCode, Bundle resultData){
        if(mReceiver!=null){
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
