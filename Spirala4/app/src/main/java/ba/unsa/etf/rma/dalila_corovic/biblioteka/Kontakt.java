package ba.unsa.etf.rma.dalila_corovic.biblioteka;

/**
 * Created by Dalija on 26.5.2018..
 */

public class Kontakt {
    public String ime;
    public String email;

    public Kontakt(String ime, String email) {
        this.ime = ime;
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
