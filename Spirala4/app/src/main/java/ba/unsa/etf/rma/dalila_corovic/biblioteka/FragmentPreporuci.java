package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Dalija on 24.5.2018..
 */

public class FragmentPreporuci extends Fragment {
    Button posalji;
    Spinner spin;
    ImageView slikaKnjige;
    TextView autor, naziv, opis;
    ArrayList<Kontakt> kontakti = new ArrayList<>();
    CustomSpinner aa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preporuci, containet, false);

        posalji = (Button) view.findViewById(R.id.dPosalji);
        spin = (Spinner) view.findViewById(R.id.sKontakti);
        slikaKnjige = (ImageView) view.findViewById(R.id.slikaD);
        autor = (TextView) view.findViewById(R.id.autor);
        naziv = (TextView) view.findViewById(R.id.naziv);
        opis = (TextView) view.findViewById(R.id.opis);

        Bundle bundle = getArguments();
        opis.setText("Opis knjige: " + bundle.getString("opis"));
        naziv.setText(bundle.getString("naziv knjige"));
        autor.setText(bundle.getString("ime autora"));

        byte[] byteArray = bundle.getByteArray("image");
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        slikaKnjige.setImageBitmap(bmp);

        ArrayList<String> names = new ArrayList<String>();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {
                    String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                    if (email != null) {
                        kontakti.add(new Kontakt(name, email));
                    }
                }
                cur1.close();
            }
        }

        aa = new CustomSpinner(getActivity(), kontakti);
        spin.setAdapter(aa);

        posalji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    int poz = spin.getSelectedItemPosition();
                    Kontakt kontakt = kontakti.get(poz);
                    String ime = kontakt.getIme();
                    String mail = kontakt.getEmail();
                    String imeKnjige = naziv.getText().toString();
                    String imeAutora = autor.getText().toString();

                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, mail);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Preporuka za knjigu");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Zdravo " + ime + ",\nPročitaj knjigu " + imeKnjige + " od " + imeAutora +"!");
                    emailIntent.setType("plain/text");
                    startActivity(emailIntent);

            }
        });

        return view;
    }
}
