package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

public class CustomListView extends BaseAdapter {
    private Context kontekst;
    ArrayList<Knjiga> pomocna;

    public interface IPreporuci{
        public void onClickedPreporuci(Knjiga k);
    }

    IPreporuci p;

    public CustomListView(Context context, ArrayList<Knjiga> data) {
        this.kontekst = context;
        this.pomocna=data;
    }

    public CustomListView(Context context, ArrayList<Knjiga> data, IPreporuci p) {
        this.kontekst = context;
        this.pomocna=data;
        this.p = p;
    }

    @Override
    public int getCount() {
        return pomocna.size();
    }

    @Override
    public Object getItem(int i) {
        return pomocna.get(i).getAutor();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) kontekst.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.element_liste_knjiga, null);
            }

            TextView tt1 = (TextView) convertView.findViewById(R.id.eDatumObjavljivanja);
            TextView tt2 = (TextView) convertView.findViewById(R.id.eOpis);
            TextView tt3 = (TextView) convertView.findViewById(R.id.eBrojStranica);
            ImageView im = (ImageView) convertView.findViewById(R.id.eNaslovna);
            Button preporuka = (Button) convertView.findViewById(R.id.dPreporuci);


            int tmp1 = pomocna.get(position).getBrojStranica();
            String tmp = String.valueOf(tmp1);

            tt1.setText("Datum objavljivanja: " + pomocna.get(position).getDatumObjavljivanja());
            tt2.setText("Opis: " + pomocna.get(position).getOpis());
            tt3.setText("Br. stranica:" + tmp);
            im.setImageBitmap(pomocna.get(position).getSlikanaslovna());

            preporuka.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    p.onClickedPreporuci(pomocna.get(position));
                }
            });


            if (pomocna.get(position).getBackground()==1) convertView.setBackgroundColor(0xffaabbed);

            return convertView;
        }

}




