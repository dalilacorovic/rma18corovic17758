package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.BR_STR;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.DATUM_OBJAVLJIVANJA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_AUTORA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_KAT;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_KNJIGE;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.IME;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.OPIS;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC3;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.SLIKA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.WEB_SERVIS;

/**
 * Created by Dalija on 20.5.2018..
 */

public class FragmentOnline extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MojResultReceiver.Receiver {
    Spinner kategorije;
    EditText upit;
    Spinner rezultat;
    Button pokreni;
    Button dodaj;
    Button povratak;
    private OnBackPress1 pomocna;
    ArrayList<Knjiga> naziviKnjiga = new ArrayList<>();
    Custom aa2;
    ArrayAdapter aa;
    ArrayList<String> listica = new ArrayList<String>();
    int poz = 0;

    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_online, containet, false);
        kategorije = (Spinner) view.findViewById(R.id.sKategorije);
        upit = (EditText) view.findViewById(R.id.tekstUpit);
        rezultat = (Spinner) view.findViewById(R.id.sRezultat);
        pokreni = (Button) view.findViewById(R.id.dRun);
        dodaj = (Button) view.findViewById(R.id.dAdd);
        povratak = (Button) view.findViewById(R.id.dPovratak);
        pomocna = (OnBackPress1) getActivity();

        Bundle bundle = getArguments();

        listica = bundle.getStringArrayList("kategorije");
        aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listica);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorije.setAdapter(aa);

        aa2 = new Custom(getActivity(), naziviKnjiga);
        rezultat.setAdapter(aa2);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pomocna.onBackPressed1();
            }
        });


        pokreni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = upit.getText().toString();
                if(s.contains("autor:")){
                    s = s.substring(6, s.length());
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)FragmentOnline.this).execute(s);
                }
                if(s.contains(";")){
                    ArrayList<String> temp = new ArrayList<>();
                    String[] arr = s.split(";");
                    for(int i=0; i<arr.length; i++){
                        String pomocni = arr[i].toString();
                        temp.add(pomocni);
                    }

                    for(int i=0; i<temp.size(); i++){
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(temp.get(i).toString());
                    }
                }
                if(s.contains("korisnik:")){
                    s = s.substring(9, s.length());
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    MojResultReceiver mReceiver = new MojResultReceiver(new Handler());
                    mReceiver.setmReceiver(FragmentOnline.this);

                    intent.putExtra("idKorisnika", s);
                    intent.putExtra("receiver", mReceiver);

                    getActivity().startService(intent);
                }
                else{
                    new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone)FragmentOnline.this).execute(s);
                }
            }
        });

        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                poz = rezultat.getSelectedItemPosition();
                Knjiga pom = naziviKnjiga.get(poz);
                pom.zanr = kategorije.getSelectedItem().toString();

                Long i = dodajKnjigu(pom);
                PomocnaB.listaKnjiga.add(pom);
                upit.setText("");

                Toast.makeText(getActivity(), "ID dodane knjige: " + String.valueOf(i), Toast.LENGTH_LONG).show();

            }
        });

        return view;
    }

    public void onDohvatiDone(ArrayList<Knjiga> k){
        for(int i=0; i<k.size(); i++){
            naziviKnjiga.add(k.get(i));
        }
        aa2.notifyDataSetChanged();
    }

    public void onNajnovijeDone(ArrayList<Knjiga> k){
        for(int i=0; i<k.size(); i++){
            naziviKnjiga.add(k.get(i));
        }
        aa2.notifyDataSetChanged();
    }

    public interface OnBackPress1{
        public void onBackPressed1();
    }

    public void onReceiveResult(int resultCode, Bundle resultData){
        if(resultCode == 0){
            Toast.makeText(getActivity(), "Servis je poceo s radom", Toast.LENGTH_LONG).show();
        }
        if(resultCode == 1){
            naziviKnjiga.add((Knjiga) resultData.get("result"));
            aa2.notifyDataSetChanged();
        }
        else{
            Toast.makeText(getActivity(), "Doslo je do greske!", Toast.LENGTH_LONG).show();
        }
    }

    public long dodajKnjigu(Knjiga knjiga){
        //DODAVANJE KNJIGE
        ContentValues novi = new ContentValues();
        novi.put(NAZIV2, knjiga.getNaziv());
        novi.put(OPIS, knjiga.getOpis());
        novi.put(DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        novi.put(BR_STR, knjiga.getBrojStranica());
        novi.put(WEB_SERVIS, knjiga.getZanr());

        //ID KATEGORIJE KOJA JE SELEKTOVANA NA SPINERU
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String id1 = "p";
        Cursor cc = db.rawQuery("SELECT _id FROM Kategorija WHERE naziv = ?", new String[]{knjiga.getZanr()});

        if(cc.getCount() > 0) {
            cc.moveToFirst();
            id1 = cc.getString(cc.getColumnIndex(PRIMARNI_KLJUC1));
        }
        cc.close();
        //
        novi.put(ID_KAT, id1);

        if(knjiga.getSlika() == null){
            String ss = BitMapToString(knjiga.getSlikanaslovna());
            novi.put(SLIKA, ss);
        } else{
            novi.put(SLIKA, knjiga.getSlika().toString());
        }

        db.insert(BazaOpenHelper.DATABASE_TABLE2, null, novi);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //DODAVANJE AUTORA
        ContentValues novi1 = new ContentValues();
        novi1.put(IME, knjiga.getAutor());
        db.insert(BazaOpenHelper.DATABASE_TABLE3, null, novi1);

        //DOHVATI ID KNJIGE
        String idK = "p";
        Cursor cursor = db.rawQuery("SELECT _id FROM Knjiga WHERE naziv = ?", new String[]{knjiga.getNaziv()});

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            idK = cursor.getString(cursor.getColumnIndex(PRIMARNI_KLJUC2));
        }
        cursor.close();

        //DOHVATI ID AUTORA
        String idA = "p";
        Cursor cA = db.rawQuery("SELECT _id FROM Autor WHERE ime = ?", new String[]{knjiga.getAutor()});

        if(cA.getCount() > 0) {
            cA.moveToFirst();
            idA = cA.getString(cA.getColumnIndex(PRIMARNI_KLJUC3));
        }
        cA.close();

        //DODAVANJE AUTORSTVA
        ContentValues novi2 = new ContentValues();
        novi2.put(ID_AUTORA, idA);
        novi2.put(ID_KNJIGE, idK);
        db.insert(BazaOpenHelper.DATABASE_TABLE4, null, novi2);
        //

        return Long.valueOf(idK);
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
