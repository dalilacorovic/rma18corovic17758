package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Dalija on 20.5.2018..
 */

public class KnjigePoznanika extends IntentService {
    public static int STATUS_START = 0;
    public static int STATUS_FINISH = 1;
    public static int STATUS_ERROR = 2;
    ArrayList<Knjiga> pom = new ArrayList<>();

    public KnjigePoznanika(){
        super(null);
    }

    public KnjigePoznanika(String name){
        super(name);
    }

    public void onCreate(){
        super.onCreate();
    }

    protected void onHandleIntent(Intent intent){
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        String ID = bundle.getString("idKorisnika");
        String url1 = "https://www.googleapis.com/books/v1/users/" + ID + "/bookshelves";

        try{
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for(int i=0; i<items.length(); i++){
                JSONObject knjiga = items.getJSONObject(i);
                String id, opis, datum, naziv;
                int brStr;
                URL slika;
                JSONArray autoripom;

                if(knjiga.has("id")) {
                    id = knjiga.getString("id");
                }else{
                    id = null;
                }

                if(knjiga.has("description")) {
                    opis = knjiga.getString("description");
                }else{
                    opis = null;
                }

                if(knjiga.has("publishedDate")){
                    datum = knjiga.getString("publishedDate");
                }else{
                    datum = null;
                }

                if(knjiga.has("pageCount")){
                    brStr = knjiga.getInt("pageCount");
                }else{
                    brStr = 0;
                }

                if(knjiga.has("title")){
                    naziv = knjiga.getString("title");
                }else{
                    naziv = null;
                }

                JSONObject zaSliku;
                if(knjiga.has("imageLinks")){
                    zaSliku = knjiga.getJSONObject("imageLinks");
                    slika = new URL(zaSliku.getString("thumbnail"));
                } else {
                    slika = null;
                }

                ArrayList<Autor> autori = new ArrayList<>();

                if(knjiga.has("authors")){
                    autoripom = knjiga.getJSONArray("authors");
                    for(int j=0; j<autoripom.length(); j++){
                        String imeA = autoripom.get(j).toString();
                        autori.add(new Autor(imeA, id));
                    }
                }else{
                    autori = null;
                }

                pom.add(new Knjiga(id, naziv, autori, opis, datum, slika, brStr));
            }

            receiver.send(STATUS_START, Bundle.EMPTY);
            bundle.putParcelableArrayList("result", pom);

            receiver.send(STATUS_FINISH, bundle);
        } catch (MalformedURLException e){
            receiver.send(STATUS_ERROR, bundle.EMPTY);
        } catch (IOException e){
            receiver.send(STATUS_ERROR, bundle.EMPTY);
        } catch (JSONException e){
            receiver.send(STATUS_ERROR, bundle.EMPTY);
        }
    }
    public String convertStreamToString (InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e){
        } finally {
            try{
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }
}
