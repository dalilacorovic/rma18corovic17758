package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import java.util.ArrayList;

/**
 * Created by Dalija on 19.5.2018..
 */

public class Autor {
    public String imeiPrezime;
    public ArrayList<String> knjige = new ArrayList<>();
    public Autor(String ime){
        imeiPrezime=ime;
    }

    public Autor(String imeiPrezime, String idKnjige) {
        this.imeiPrezime = imeiPrezime;
        knjige.add(idKnjige);
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setImeiPrezime(String imeiPrezime) {
        this.imeiPrezime = imeiPrezime;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    public void dodajKnjigu(String id){
        knjige.add(String.valueOf(id));
    }
}
