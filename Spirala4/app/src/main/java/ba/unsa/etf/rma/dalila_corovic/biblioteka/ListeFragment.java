package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.util.ArrayList;

import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.DATABASE_TABLE1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC1;

public class ListeFragment extends Fragment {
    Button dugme1;
    Button dugme2;
    Button dugme3;
    Button dugme4;
    Button dugme5;
    Button dodajOnline;
    EditText tekst;
    ListView lista;
    ArrayList<String> unosi; //unos kategorija
    ArrayAdapter<String> adapter, adapter1;
    private OnButtonClick pom;
    private OnButtonClick2 pom2;
    private OnListClick pom1;

    //​@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState) {
        View view = inflater.inflate(R.layout.lista_fragment, container, false);

        dugme2 = (Button) view.findViewById(R.id.dPretraga); //PRETRAGA
        dugme1 = (Button) view.findViewById(R.id.dDodajKategoriju); //DODAVANJE KATEGORIJE
        dugme3 = (Button) view.findViewById(R.id.dDodajKnjigu); //DODAJ KNJIGU
        dugme4 = (Button) view.findViewById(R.id.dKategorije); //PREGLED KATEGORIJA
        dugme5 = (Button) view.findViewById(R.id.dAutori); //PREGLED AUTORA
        tekst = (EditText) view.findViewById(R.id.tekstPretraga); //PRETRAGA TEKSTA
        lista = (ListView) view.findViewById(R.id.listaKategorija); //PREGLED LISTE
        dodajOnline = (Button) view.findViewById(R.id.dDodajOnline);
        pom=(OnButtonClick)getActivity();
        pom1=(OnListClick)getActivity();
        pom2=(OnButtonClick2) getActivity();


        KlasaKategorija.unosi.clear();
        adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, KlasaKategorija.unosi);
        lista.setAdapter(adapter);

        String[] koloneRezultat = new String[]{PRIMARNI_KLJUC1, NAZIV1};
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        Cursor cursor = db.query(BazaOpenHelper.DATABASE_TABLE1, koloneRezultat, null, null, null, null, null);

        int imena = cursor.getColumnIndexOrThrow(NAZIV1);
        ArrayList<String> tmp = new ArrayList<>();
        while(cursor.moveToNext()){
            KlasaKategorija.unosi.add(cursor.getString(imena));
            adapter.notifyDataSetChanged();
        }
        cursor.close();

        if(KlasaKategorija.unosi.size()==0){
            dugme1.setEnabled(false);
        }

        //PRETRAGA - filtriranje
        dugme2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = tekst.getText().toString();
                (ListeFragment.this).adapter.getFilter().filter(string);

                dugme1.setEnabled(true);

            }
        });

        //DODAJ KATEGORIJU
        dugme1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pom = tekst.getText().toString();
                Boolean ima = CheckIsDataAlreadyInDBorNot(pom);
                if(ima == false){
                    Long i = dodajKategoriju(pom);

                    KlasaKategorija.unosi.add(tekst.getText().toString());
                    tekst.setText("");
                    adapter.notifyDataSetChanged();
                    Toast.makeText(getActivity(), "Id dodane kat.: " + i.toString(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Kategorija vec postoji!", Toast.LENGTH_LONG).show();
                }

            }
        });

        //otvaranje drugog fargmenta
        dugme3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom.OnButtonClicked(KlasaKategorija.unosi);
            }
        });

        dodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom2.OnButtonClicked2(KlasaKategorija.unosi);
            }
        });

        //Lista autora
        dugme5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dugme2.setVisibility(View.GONE);
                dugme1.setVisibility(View.GONE);
                tekst.setVisibility(View.GONE);
                int broj;

                ArrayList<String> pomocniAutori = new ArrayList<>();

                int duzina = PomocnaB.listaKnjiga.size();
                for (int i = 0; i < duzina; i++) {
                    pomocniAutori.add(PomocnaB.listaKnjiga.get(i).getAutor());
                }

                for (int i = 0; i < duzina; i++) {
                    broj=1;
                    String pom1;

                    for (int j = i + 1; j < duzina; j++) {
                        if (pomocniAutori.get(i).contentEquals(pomocniAutori.get(j))) {
                            pomocniAutori.remove(j);
                            j--;
                            broj++;
                            duzina--;
                        }
                    }
                    pom1 = String.valueOf(broj);

                    pomocniAutori.get(i).replace(pomocniAutori.get(i), pomocniAutori.get(i) + ", broj knjiga: " + pom1);
                    pomocniAutori.set(i, pomocniAutori.get(i) + ", broj knjiga: " + pom1);
                }

                adapter1 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, pomocniAutori);
                lista.setAdapter(adapter1);
            }

        });

        dugme4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!dugme2.isShown() && !dugme1.isShown() && !tekst.isShown()) {
                    dugme2.setVisibility(View.VISIBLE);
                    dugme1.setVisibility(View.VISIBLE);
                    tekst.setVisibility(View.VISIBLE);
                }
                adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, KlasaKategorija.unosi);
                lista.setAdapter(adapter);
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pom1.OnListClicked(KlasaKategorija.unosi.get(i).toString());
            }
        });

        return view;
    }

    public long dodajKategoriju(String naziv) {
        ContentValues novi = new ContentValues();
        novi.put(NAZIV1, naziv);
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();

        db.insert(BazaOpenHelper.DATABASE_TABLE1, null, novi);

        //Dohvati id dodane kategorije
        long id = 5050;
        Cursor cursor = db.rawQuery("SELECT _id FROM Kategorija WHERE naziv = ?", new String[]{naziv});

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getLong(cursor.getColumnIndex(PRIMARNI_KLJUC1));
        }
        cursor.close();

        return id;
    }

    public boolean CheckIsDataAlreadyInDBorNot(String fieldValue) {
        for(int i=0; i<KlasaKategorija.unosi.size(); i++){
            if(KlasaKategorija.unosi.get(i).equals(fieldValue)){
                return true;
            }
        }
        return false;
    }

    public interface OnButtonClick {
        public void OnButtonClicked(ArrayList<String> u);
    }

    public interface OnListClick {
        public void OnListClicked(String poz);
    }

    public interface OnButtonClick2{
        public void OnButtonClicked2(ArrayList<String> u);
    }

}





