package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.AndroidRuntimeException;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.BR_STR;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.DATUM_OBJAVLJIVANJA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_AUTORA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_KAT;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.IME;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.OPIS;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.SLIKA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.WEB_SERVIS;

/**
 * Created by Dalija on 11.4.2018..
 */

public class KnjigeFragment extends Fragment implements CustomListView.IPreporuci{
    Button povratak;
    ListView lista;
    private OnBackPress pom;
    ArrayList<Knjiga> knjigekategorije = new ArrayList<>();

    public void onClickedPreporuci(Knjiga k) {
        oc.onClickedPreporuci(k);
    }

    public interface onClick{
        public void onClickedPreporuci(Knjiga k);
    }
    onClick oc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_lista_knjiga_akt, containet, false);

        povratak = (Button)view.findViewById(R.id.dPovratak);
        lista = (ListView)view.findViewById(R.id.listaKnjiga);
        pom=(OnBackPress)getActivity();
        oc=(onClick) getActivity();

        Bundle bundle = getArguments();
        String kategorija = bundle.getString("kategorija");

        //ID KATEGORIJE
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        long id1 = 5050;
        Cursor cursor = db.rawQuery("SELECT _id FROM Kategorija WHERE naziv = ?", new String[]{kategorija});

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            id1 = cursor.getLong(cursor.getColumnIndex(PRIMARNI_KLJUC1));
        }
        cursor.close();

        ///////////////
        knjigekategorije.clear();
        knjigekategorije.addAll(knjigeKategorije(id1));

        final CustomListView adapter = new CustomListView(this.getActivity(), knjigekategorije, this);
        lista.setAdapter(adapter);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom.onBackPressed();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setBackgroundColor(getResources().getColor(R.color.SvijetloPlava));
                knjigekategorije.get(i).setBackground(1);
                adapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    public ArrayList<Knjiga> knjigeKategorije(long idK){
        ArrayList<Knjiga> vrijednost = new ArrayList<>();

        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();

        String s = String.valueOf(idK);
        Cursor cursor = db.rawQuery("SELECT * FROM Knjiga WHERE idKategorije = ?", new String[]{s});

        if(cursor.getCount() > 0) {
            while(cursor.moveToNext()){

                String idKnj = cursor.getString(cursor.getColumnIndex(PRIMARNI_KLJUC2));
                String aauuttoorr = nadjiAutora(idKnj);
                String naslov = cursor.getString(cursor.getColumnIndex(NAZIV2));
                String ops = cursor.getString(cursor.getColumnIndex(OPIS));
                String datum = cursor.getString(cursor.getColumnIndex(DATUM_OBJAVLJIVANJA));
                String zanrr = cursor.getString(cursor.getColumnIndex(ID_KAT));
                int br = Integer.valueOf(cursor.getString(cursor.getColumnIndex(BR_STR)));
                if(cursor.getString(cursor.getColumnIndex(SLIKA)).contains("http")){
                    try {
                        String sss = cursor.getString(cursor.getColumnIndex(SLIKA));
                        URL url = new URL(sss);
                        ArrayList<Autor> aaa = new ArrayList<>();
                        aaa.add(new Autor(aauuttoorr));
                        vrijednost.add(new Knjiga(idKnj, naslov, aaa, ops, datum, url, br));
                    } catch (MalformedURLException e){

                    }

                } else {
                    Bitmap slikica = StringToBitMap(cursor.getString(cursor.getColumnIndex(SLIKA)));
                    vrijednost.add(new Knjiga(aauuttoorr, naslov, zanrr, slikica));
                }

            }
        }
        cursor.close();

        return vrijednost;
    }

    public String nadjiAutora(String idK){
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getReadableDatabase();

        //NADJEMO ID AUTORA
        String idA = "p";
        Cursor c1 = db.rawQuery("SELECT idAutora FROM Autorstvo WHERE idKnjige =  ?", new String[]{idK});
        if(c1.getCount()>0){
            c1.moveToFirst();
            idA = c1.getString(c1.getColumnIndex(ID_AUTORA));
        }
        c1.close();

        //NADJIMO IME AUTORA
        String nameA = "p";
        Cursor c22 = db.rawQuery("SELECT ime FROM Autor where _id = ?", new String[]{idA});
        if(c22.getCount()>0){
            c22.moveToFirst();
            nameA = c22.getString(c22.getColumnIndex(IME));
        }
        c22.close();

        return nameA;
    }

    public interface OnBackPress{
        public void onBackPressed();
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }

}
