package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.icu.util.BuddhistCalendar;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.BR_STR;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.DATUM_OBJAVLJIVANJA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_AUTORA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_KAT;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.ID_KNJIGE;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.IME;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.NAZIV2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.OPIS;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC1;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC2;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.PRIMARNI_KLJUC3;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.SLIKA;
import static ba.unsa.etf.rma.dalila_corovic.biblioteka.BazaOpenHelper.WEB_SERVIS;


public class DodavanjeKnjigeFragment extends Fragment {
    static final int PICK_IMAGE_REQUEST = 1;
    ImageView slika;
    EditText autor;
    EditText knjiga;
    Button nadjisliku;
    Button upisiknjigu;
    Spinner spin;
    Button ponisti;
    ArrayAdapter aa;
    private OnBackPress pom;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.activity_dodavanje_knjige_akt, containet, false);

        slika = (ImageView)view.findViewById(R.id.naslovnaStr);
        autor = (EditText)view.findViewById(R.id.imeAutora);
        knjiga = (EditText)view.findViewById(R.id.nazivKnjige);
        nadjisliku = (Button)view.findViewById(R.id.dNadjiSliku);
        upisiknjigu = (Button)view.findViewById(R.id.dUpisiKnjigu);
        spin = (Spinner)view.findViewById(R.id.sKategorijaKnjige);
        ponisti = (Button)view.findViewById(R.id.dPonisti);
        pom = (OnBackPress)getActivity();


        Bundle bundle = getArguments();
        ArrayList<String> listica = new ArrayList<String>();
        listica = bundle.getStringArrayList("kategorije");
        aa = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listica);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        upisiknjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String i; //ime autora
                String k; //knjiga
                String z; //zanr-kategorija
                Bitmap b;
                i = autor.getText().toString(); //IME AUTORA
                k = knjiga.getText().toString(); //IME KNJIGE
                z = spin.getSelectedItem().toString();

                slika.setDrawingCacheEnabled(true);
                slika.buildDrawingCache();
                b = Bitmap.createBitmap(slika.getDrawingCache());
                Knjiga pom = new Knjiga(i, k, z, b);

                if(imaLiKnjige(k) == true){
                    Toast.makeText(getActivity(), "Knjiga je vec unesena!", Toast.LENGTH_LONG).show();
                } else {
                    Long idid = dodajKnjigu(pom);

                    PomocnaB.listaKnjiga.add(pom);
                    autor.setText("");
                    knjiga.setText("");
                    Toast.makeText(getActivity(), "Id dodane knjige: " + idid.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });

        nadjisliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivity(intent);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom.onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        DodavanjeKnjigeFragment.super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                slika.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public long dodajKnjigu(Knjiga knjiga){
        //DODAVANJE KNJIGE
        ContentValues novi = new ContentValues();
        novi.put(NAZIV2, knjiga.getNaziv());
        novi.put(OPIS, knjiga.getOpis());
        novi.put(DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
        novi.put(BR_STR, knjiga.getBrojStranica());
        novi.put(WEB_SERVIS, knjiga.getZanr());

        //ID KATEGORIJE KOJA JE SELEKTOVANA NA SPINERU
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String id1 = "p";
        Cursor cc = db.rawQuery("SELECT _id FROM Kategorija WHERE naziv = ?", new String[]{knjiga.getZanr()});

        if(cc.getCount() > 0) {
            cc.moveToFirst();
            id1 = cc.getString(cc.getColumnIndex(PRIMARNI_KLJUC1));
        }
        cc.close();
        //
        novi.put(ID_KAT, id1);

        if(knjiga.getSlika() == null){
            String ss = BitMapToString(knjiga.getSlikanaslovna());
            novi.put(SLIKA, ss);
        } else{
            novi.put(SLIKA, knjiga.getSlika().toString());
        }

        db.insert(BazaOpenHelper.DATABASE_TABLE2, null, novi);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //DODAVANJE AUTORA
        ContentValues novi1 = new ContentValues();
        novi1.put(IME, knjiga.getAutor());
        db.insert(BazaOpenHelper.DATABASE_TABLE3, null, novi1);

        //DOHVATI ID KNJIGE
        String idK = "p";
        Cursor cursor = db.rawQuery("SELECT _id FROM Knjiga WHERE naziv = ?", new String[]{knjiga.getNaziv()});

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            idK = cursor.getString(cursor.getColumnIndex(PRIMARNI_KLJUC2));
        }
        cursor.close();

        //DOHVATI ID AUTORA
        String idA = "p";
        Cursor cA = db.rawQuery("SELECT _id FROM Autor WHERE ime = ?", new String[]{knjiga.getAutor()});

        if(cA.getCount() > 0) {
            cA.moveToFirst();
            idA = cA.getString(cA.getColumnIndex(PRIMARNI_KLJUC3));
        }
        cA.close();

        //DODAVANJE AUTORSTVA
        ContentValues novi2 = new ContentValues();
        novi2.put(ID_AUTORA, idA);
        novi2.put(ID_KNJIGE, idK);
        db.insert(BazaOpenHelper.DATABASE_TABLE4, null, novi2);
        //

        return Long.valueOf(idK);
    }

    public boolean imaLiKnjige(String aaa){
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getReadableDatabase();

        Cursor cc = db.rawQuery("SELECT naziv FROM Knjiga WHERE naziv = ?", new String[]{aaa});
        if(cc.getCount()>0){
            cc.close();
            return true;
        } else {
            cc.close();
            return false;
        }
    }

    public interface OnBackPress{
        public void onBackPressed();
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
