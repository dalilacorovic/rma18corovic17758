package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Dalija on 27.3.2018..
 */

public class Knjiga implements Parcelable{
    public String autor;
    public String nazivknjige;
    public String zanr;
    public Bitmap slikanaslovna;
    public int background;

    public String id;
    public String naziv;
    public ArrayList<Autor> autori;
    public String opis;
    public String datumObjavljivanja;
    public URL slika;
    public int brojStranica;

    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slikaa, int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = autori;
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slikaa;
        this.brojStranica = brojStranica;
        this.autor=autori.get(0).toString();
        this.nazivknjige=naziv;
        this.zanr=" ";
        this.slikanaslovna=getBitmapFromURL(slikaa.toString());
        this.background=0;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Autor> getAutori() {
        return autori;
    }

    public void setAutori(ArrayList<Autor> autori) {
        this.autori = autori;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public URL getSlika() {
        return slika;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public Knjiga(String autor, String nazivknjige, String zanr, Bitmap slikanaslovna) {
        this.autor = autor;
        this.nazivknjige = nazivknjige;
        this.zanr = zanr;
        this.slikanaslovna = slikanaslovna;
        this.background=0;

        this.id = "Nije unesen";
        this.naziv = nazivknjige;
        this.autori = null;
        this.opis = "Nije unesen";
        this.datumObjavljivanja = "Nije unesen";
        this.brojStranica = 0;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getNazivknjige() {
        return nazivknjige;
    }

    public void setNazivknjige(String nazivknjige) {
        this.nazivknjige = nazivknjige;
    }

    public String getZanr() {
        return zanr;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }

    public Bitmap getSlikanaslovna() {
        return slikanaslovna;
    }

    public void setSlikanaslovna(Bitmap slikanaslovna) {
        this.slikanaslovna = slikanaslovna;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);

            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags){
        dest.writeString(id);
        dest.writeString(naziv);
        dest.writeString(autori.get(0).toString());
        dest.writeString(opis);
        dest.writeString(slika.toExternalForm());
    }
}
