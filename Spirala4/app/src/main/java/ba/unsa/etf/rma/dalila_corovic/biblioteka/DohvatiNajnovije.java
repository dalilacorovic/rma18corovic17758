package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Dalija on 20.5.2018..
 */

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {
    public interface IDohvatiNajnovijeDone{
        public void onNajnovijeDone(ArrayList<Knjiga> k);
    }

    ArrayList<Knjiga> k = new ArrayList<>();
    private IDohvatiNajnovijeDone pozivatelj;
    public DohvatiNajnovije(IDohvatiNajnovijeDone p) { pozivatelj = p; };

    protected Void doInBackground(String... params){
        String query = params[0];
        try{
            query = URLEncoder.encode(params[0], "utf-8");
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }

        String url1 = "https://www.googleapis.com/books/v1/volumes?q=" + query + "&maxResults=5&orderBy=newest";

        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            JSONArray items = jo.getJSONArray("items");

            for(int i=0; i<items.length(); i++){
                JSONObject knjiga = items.getJSONObject(i);
                JSONObject info = knjiga.getJSONObject("volumeInfo");
                String id, opis, datum, naziv;
                int brStr;
                JSONArray autoripom;

                if(info.has("id")) {
                    id = info.getString("id");
                }else{
                    id = null;
                }

                if(info.has("description")) {
                    opis = info.getString("description");
                }else{
                    opis = null;
                }

                if(info.has("publishedDate")){
                    datum = info.getString("publishedDate");
                }else{
                    datum = null;
                }

                if(info.has("pageCount")){
                    brStr = info.getInt("pageCount");
                }else{
                    brStr = 0;
                }

                if(info.has("title")){
                    naziv = info.getString("title");
                }else{
                    naziv = null;
                }

                JSONObject zaSliku = info.getJSONObject("imageLinks");
                URL slika = new URL(zaSliku.getString("thumbnail"));

                ArrayList<Autor> autori = new ArrayList<>();

                if(info.has("authors")){
                    autoripom = info.getJSONArray("authors");
                }else{
                    autoripom = null;
                }

                for(int j=0; j<autoripom.length(); j++){
                    String imeA = autoripom.get(j).toString();
                    autori.add(new Autor(imeA, id));
                }

                k.add(new Knjiga(id, naziv, autori, opis, datum, slika, brStr));
        }
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        pozivatelj.onNajnovijeDone(k);
    }

    public String convertStreamToString (InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try{
            while((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e){
        } finally {
            try{
                is.close();
            } catch (IOException e){
            }
        }
        return sb.toString();
    }
}
