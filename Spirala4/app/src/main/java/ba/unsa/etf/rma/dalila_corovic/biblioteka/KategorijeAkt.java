package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.icu.util.BuddhistCalendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.stetho.Stetho;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;


public class KategorijeAkt extends AppCompatActivity implements KnjigeFragment.onClick,ListeFragment.OnButtonClick2, FragmentOnline.OnBackPress1, ListeFragment.OnButtonClick, DodavanjeKnjigeFragment.OnBackPress, KnjigeFragment.OnBackPress, ListeFragment.OnListClick{
    Button dugme1;
    Button dugme2;
    Button dugme3;
    Button dugmeonline;
    EditText tekst;
    ListView lista;
    ArrayAdapter<String> adapter;
    ArrayList<String> unosi;
    FragmentManager fm = getFragmentManager();
    Boolean siril=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);

        dugme1 = (Button) findViewById(R.id.dDodajKategoriju);
        dugme2 = (Button) findViewById(R.id.dPretraga);
        dugme3 = (Button) findViewById(R.id.dDodajKnjigu);
        tekst = (EditText) findViewById(R.id.tekstPretraga);
        lista = (ListView) findViewById(R.id.listaKategorija);
        dugmeonline = (Button) findViewById(R.id.dDodajOnline);

        FragmentManager fm = getFragmentManager();
        FrameLayout ldeatelji = (FrameLayout)findViewById(R.id.mjestoF2);
        if(ldeatelji!=null){
            siril = true;
            KnjigeFragment lf = (KnjigeFragment) fm.findFragmentById(R.id.mjestoF2);
            if(lf==null){
                lf = new KnjigeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("kategorija", "kat");
                lf.setArguments(bundle);

                fm.beginTransaction().replace(R.id.mjestoF2, lf).commit();
            }
            else {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }

        ListeFragment kf = (ListeFragment)fm.findFragmentByTag("Lista");
        if(kf==null){
            kf = new ListeFragment();
        FragmentPreporuci fp = new FragmentPreporuci();
            fm.beginTransaction().replace(R.id.prikaziFragment, kf, "Lista").commit();
        }

    }

    @Override
    public void OnButtonClicked(ArrayList<String> unos){
        DodavanjeKnjigeFragment dkf = new DodavanjeKnjigeFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("kategorije", unos);
        dkf.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, dkf).addToBackStack("prva").commit();

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void OnListClicked(String s){
        KnjigeFragment kf = new KnjigeFragment();
        Bundle bundle = new Bundle();
        bundle.putString("kategorija", s);
        kf.setArguments(bundle);

        if(siril){
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, kf).commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, kf).addToBackStack("druga").commit();
        }

    }

    @Override
    public void OnButtonClicked2(ArrayList<String> unos){
        FragmentOnline fo = new FragmentOnline();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("kategorije", unos);
        fo.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, fo).addToBackStack("treci").commit();
    }

    @Override
    public void onBackPressed1(){
        getFragmentManager().popBackStack();
    }

    public void onClickedPreporuci(Knjiga k){
        FragmentPreporuci fp = new FragmentPreporuci();
        Bundle bundle = new Bundle();
        bundle.putString("ime autora", k.getAutor());

        bundle.putString("naziv knjige", k.getNaziv());
        bundle.putString("opis", k.getOpis());
        Bitmap bb = k.getSlikanaslovna();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bb.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bundle.putByteArray("image",byteArray);

        fp.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.prikaziFragment, fp).addToBackStack(null).commit();
    }

}
