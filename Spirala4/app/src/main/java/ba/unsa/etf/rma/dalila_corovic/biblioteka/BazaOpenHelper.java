package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Dalija on 27.5.2018..
 */

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final String DATABASE_TABLE1 = "Kategorija";
    public static final String DATABASE_TABLE2 = "Knjiga";
    public static final String DATABASE_TABLE3 = "Autor";
    public static final String DATABASE_TABLE4 = "Autorstvo";
    public static final int DATABASE_VERSION = 1;
    public static final String PRIMARNI_KLJUC1 = "_id";
    public static final String NAZIV1 = "naziv";
    public static final String PRIMARNI_KLJUC2 = "_id";
    public static final String NAZIV2 = "naziv";
    public static final String OPIS = "opis";
    public static final String DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String BR_STR = "brojStranica";
    public static final String WEB_SERVIS = "idWebServis";
    public static final String ID_KAT = "idKategorije";
    public static final String PRIMARNI_KLJUC3 = "_id";
    public static final String IME = "ime";
    public static final String PRIMARNI_KLJUC4 = "_id";
    public static final String ID_AUTORA = "idAutora";
    public static final String ID_KNJIGE = "idKnjige";
    public static final String SLIKA = "slika";

    public static final String CREATE_TABLE1 = "create table " + DATABASE_TABLE1 + " (" + PRIMARNI_KLJUC1
            + " integer primary key autoincrement, " + NAZIV1 + " text not null unique);";

    public static final String CREATE_TABLE2 = "create table " + DATABASE_TABLE2 + " (" + PRIMARNI_KLJUC2 +
            " integer primary key autoincrement, " + NAZIV2 + " text not null, " + OPIS + " text, " +
            DATUM_OBJAVLJIVANJA + " text, " + BR_STR + " integer, " +  WEB_SERVIS + " text, " + ID_KAT
            + " integer, " + SLIKA + " text);";

    public static final String CREATE_TABLE3 = "create table " + DATABASE_TABLE3 + " (" + PRIMARNI_KLJUC3 +
            " integer primary key autoincrement, " + IME + " text);";


    public static final String CREATE_TABLE4 = "create table " + DATABASE_TABLE4 + " (" + PRIMARNI_KLJUC4 +
            " integer primary key autoincrement, " + ID_AUTORA + " integer, " + ID_KNJIGE + " integer);";

    public BazaOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE1);
        db.execSQL(CREATE_TABLE2);
        db.execSQL(CREATE_TABLE3);
        db.execSQL(CREATE_TABLE4);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE1);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE2);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE3);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE4);
        onCreate(db);
    }
}
