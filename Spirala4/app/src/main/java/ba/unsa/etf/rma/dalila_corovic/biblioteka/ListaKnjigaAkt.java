package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaKnjigaAkt extends AppCompatActivity {
    Button povratak;
    ListView lista;
    ArrayList<Knjiga> knjigekategorije;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);

        povratak = (Button)findViewById(R.id.dPovratak);
        lista = (ListView)findViewById(R.id.listaKnjiga);
        knjigekategorije = new ArrayList<>();

        String kategorija = getIntent().getExtras().getString("kategorija");
        for(int i=0; i<PomocnaB.listaKnjiga.size(); i++){
           if(PomocnaB.listaKnjiga.get(i).getZanr().contentEquals(kategorija)) {
                Knjiga k = PomocnaB.listaKnjiga.get(i);
                knjigekategorije.add(k);
           }
        }

        final CustomListView adapter = new CustomListView(getApplicationContext(), knjigekategorije);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setBackgroundColor(getResources().getColor(R.color.SvijetloPlava));
                knjigekategorije.get(i).background=1;
                adapter.notifyDataSetChanged();
            }
        });

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
