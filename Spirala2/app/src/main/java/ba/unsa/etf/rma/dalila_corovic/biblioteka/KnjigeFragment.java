package ba.unsa.etf.rma.dalila_corovic.biblioteka;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.BufferedReader;
import java.util.ArrayList;

/**
 * Created by Dalija on 11.4.2018..
 */

public class KnjigeFragment extends Fragment {
    Button povratak;
    ListView lista;
    private OnBackPress pom;
    ArrayList<Knjiga> knjigekategorije = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containet, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_lista_knjiga_akt, containet, false);

        povratak = (Button)view.findViewById(R.id.dPovratak);
        lista = (ListView)view.findViewById(R.id.listaKnjiga);
        pom=(OnBackPress)getActivity();

        Bundle bundle = getArguments();
        String kategorija = bundle.getString("kategorija");

        for(int i=0; i<PomocnaB.listaKnjiga.size(); i++){
            if(PomocnaB.listaKnjiga.get(i).getZanr().contentEquals(kategorija)) {
                Knjiga k = PomocnaB.listaKnjiga.get(i);
                knjigekategorije.add(k);
            }
        }

        final CustomListView adapter = new CustomListView(getActivity().getApplicationContext(), knjigekategorije);
        lista.setAdapter(adapter);

        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pom.onBackPressed();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setBackgroundColor(R.color.SvijetloPlava);
                knjigekategorije.get(i).background=1;
                adapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    public interface OnBackPress{
        public void onBackPressed();
    }
}
